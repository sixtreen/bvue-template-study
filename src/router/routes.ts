// 路由
export const constantRoute = [{
    // 登录
    path: '/login',
    component: () => import('@/view/login/index.vue'),
    name: 'login'
}, {
    // 登录成功后的路由
    path: '/',
    component: () => import('@/layout/index.vue'),
    name: 'layout',
    redirect: '/home',
    children: [{
        path: '/home',
        component: () => import('@/view/home/index.vue'),
        name: 'home',
        meta: {
            title: '首页',
            hidden: false,
            icon: 'HomeFilled'
        }
    }]
}]