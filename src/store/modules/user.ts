//创建用户相关的小仓库
import { defineStore } from 'pinia'
import { SET_TOKEN, GET_TOKEN, REMOVE_TOKEN } from '@/utils/token'

import { constantRoute } from '@/router/routes'
import { loginFormData, loginResponseData, userInfoReponseData } from '@/api/user/type'
import { reqLogin, reqUserInfo } from '@/api/user'
import { reactive, toRefs } from 'vue'

interface userDetailData {
    username: string,
    avatar: string,
    buttons: string[]
}

let useUserStore = defineStore('User', () => {
    // 用户登录
    async function userLogin(data: loginFormData) {
        //登录请求
        let result: loginResponseData = await reqLogin(data);
        //登录请求:成功200->token 
        //登录请求:失败201->登录失败错误的信息
        if (result.code == 200) {
            //pinia仓库存储一下token
            //本地存储持久化存储一份
            SET_TOKEN((result.data as string))
            return 'ok'
        } else {
            return Promise.reject(new Error(result.data))
        }
    }

    let userDetail: userDetailData = reactive({
        username: '',
        avatar: '',
        buttons: []
    })

    // 获取用户信息
    async function userInfo() {
        //获取用户信息进行存储仓库当中[用户头像、名字]
        let result: userInfoReponseData = await reqUserInfo()
        //如果获取用户信息成功，存储一下用户信息
        if (result.code == 200) {
            userDetail.username = result.data.name
            userDetail.avatar = result.data.avatar
            userDetail.buttons = result.data.buttons as string[];
        }
    }



    return {
        token: GET_TOKEN(),//用户唯一标识token
        menuRoutes: constantRoute,//仓库存储生成菜单需要数组(路由)
        ...toRefs(userDetail),
        //存储当前用户是否包含某一个按钮
        buttons: [],
        userLogin
    }
})

export default useUserStore